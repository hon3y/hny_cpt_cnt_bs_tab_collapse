<?php

defined('TYPO3_MODE') or die();

$sModel = 'tx_teufelscptcntbstabcollapse_domain_model_tab';

$GLOBALS['TCA'][$sModel]['columns']['render_page_id']['config'] = [
    'type' => 'input',
    'size' => 20,
    'eval' => 'trim',
    'wizards' => [
        '_PADDING' => 2,
        'link' => [
            'type' => 'popup',
            'title' => 'Link',
            'icon' => 'link_popup.gif',
            'module' => [
                'name' => 'wizard_element_browser',
                'urlParameters' => [
                    'mode' => 'wizard'
                ]
            ],
            'JSopenParams' => 'height=800,width=600,status=0,menubar=0,scrollbars=1'
        ],
    ],
];